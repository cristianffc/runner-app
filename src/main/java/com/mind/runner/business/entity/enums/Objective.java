package com.mind.runner.business.entity.enums;

public enum Objective {
    FORCE,
    POTENCY,
    VELOCITY,
    RESISTANCE
}
