package com.mind.runner.business.entity.enums;

public enum Surface {
    ASPHALT,
    GRASS,
    SAND,
    TRAIL,
    SYNTHETIC,
    SNOW
}
