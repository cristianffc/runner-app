package com.mind.runner.business.entity.enums;

public enum Specialty {
    MARATHON,
    LONG_RUN,
    MIDDLE_RUN,
    SPRINT,
    SLIMMING,
    CONTEST
}
